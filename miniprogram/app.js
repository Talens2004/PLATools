//app.js

// 将如下代码添加到json文件中
// "tabBar": {
//     "list": [
//         {
//             "pagePath": "pages/zeroReport/zeroReport",
//             "text": "功能",
//             "iconPath": "icons/list_normal.png",
//             "selectedIconPath": "icons/list_highlight.png"
//         },
//         {
//             "pagePath": "pages/about/about",
//             "text": "设置",
//             "iconPath": "icons/setting_normal.png",
//             "selectedIconPath": "icons/setting_highlight.png"
//         }
//     ]
// },

App({
    onLaunch: function() {

        if (!wx.cloud) {
            console.error('请使用 2.2.3 或以上的基础库以使用云能力')
        } else {
            wx.cloud.init({
                // env 参数说明：
                //   env 参数决定接下来小程序发起的云开发调用（wx.cloud.xxx）会默认请求到哪个云环境的资源
                //   此处请填入环境 ID, 环境 ID 可打开云控制台查看
                //   如不填则使用默认环境（第一个创建的环境）
                env: 'platools-fengmang003',
                traceUser: true,
            })
        }

        this.globalData = {
            openid: ''
        }
    }
})