// zzRemind.js
// 在需要使用的js文件中，导入js
var dateUtil = require('../../utils/dateUtil.js');
var cloudUtil = require('../../utils/cloudUtil.js');

// 获取小程序实例
const app = getApp()
// 获取今天时间戳
const today = dateUtil.todayStamps()
// 获取数据库操作串
const db = wx.cloud.database()



Page({

    // 页面数据
    data: {
        userInfo: {},
        logged: false,
        takeSession: false,
        requestResult: '',
        reportedList: [],
        isEmptyList: 'none', // 异常报告输入框是否显示标志
    },

    // 页面加载时自动执行
    onLoad: function () {
        //获取openid
        cloudUtil.fetchOpenId()

        // 获取用户信息
        wx.getSetting({
            success: res => {
                if (res.authSetting['scope.userInfo']) {
                    // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
                    wx.getUserInfo({
                        success: res => {
                            console.log(res)

                            this.setData({
                                avatarUrl: res.userInfo.avatarUrl,
                                userInfo: res.userInfo
                            })
                        }
                    })
                }
            }
        })

        this.fetchGoods()
    },


})