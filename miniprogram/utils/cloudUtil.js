function fetchOpenId() {

    // 如果环境初始化失败，直接返回
    if (!wx.cloud) {
        return
    }

    if (!app.globalData.openid) {
        // 调用云函数
        wx.cloud.callFunction({
            name: 'login',
            data: {},
            success: res => {
                console.log('[云函数] [login] user openid: ', res.result.openid)
                app.globalData.openid = res.result.openid
                console.log(res)
            },
            fail: err => {
                console.error('[云函数] [login] 调用失败', err)
            }
        })
    }
}


module.exports = {
    fetchOpenId: fetchOpenId
}